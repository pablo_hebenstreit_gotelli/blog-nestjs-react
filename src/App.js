import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Admin from './components/Admin'
import User from './components/User'
import Login from './components/Login'
import loginApi from "./ApiService"

export const UserContext = React.createContext()

export default function App(props) {

  const [token, setToken] = React.useState('')
  console.log('hola desde ' + token);
  const [loginData, setLoginData] = React.useState({})
  /* const [role, setRole] = React.useState('User') */

  const parseJwt = (userToken) => {
    var base64Url = userToken.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
  }
  
  React.useEffect(()=> {
    console.log('LoginData '+ JSON.stringify(loginData))
    if(loginData.email && loginData.password){
      loginApi(loginData)
        .then(res => 
          {setToken(oldToken => {return res.access_token;})
        });
      }
      console.log('token ' + token)
    }, [loginData])
  
  return (

    <Router>

      <UserContext.Provider value={
        {loginData, setLoginData, token, setToken, parseJwt}
      }>
        

      <Switch>
        <Route path="/admin">
          <Admin setToken={setToken}/>
        </Route>
        <Route path="/user">
          <User />
        </Route>
        <Route path="/">
          <Login setToken={setToken} />
        </Route>
      </Switch>

      </UserContext.Provider>
      
    </Router>
  );
}



  /* const saveLoginData = (email, password) => {
    setLoginData(() => {return {email, password}});
  } */

const urlLogin = "http://localhost:3000/api/login";
function loginApi(loginData) {
    console.log('Hola Marta')
    return fetch(
        urlLogin, {
            body: JSON.stringify(loginData),
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
        })
        .then(response => response.json())
        .catch((err) => {console.log(err)});
}
export default loginApi;
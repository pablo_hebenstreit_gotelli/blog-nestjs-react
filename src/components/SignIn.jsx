import React from 'react'
import { UserContext } from '../App'
import loginApi from '../ApiService'
import { useHistory } from 'react-router-dom'

const SignIn = (props) => {

    const { setLoginData, setToken, parseJwt } = React.useContext(UserContext)

    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    const userData = { email, password }
    const history = useHistory();

    const signIn = async (e) => {
        e.preventDefault();
        console.log('userdatafrom form ' +  JSON.stringify(userData))
        await setLoginData({ userData })
        if(userData.email && userData.password){
           const userToken = await loginApi(userData) 
           console.log('userToken ' + JSON.stringify(userToken))
           setToken(JSON.stringify(userToken.token))
           const comparative = parseJwt(userToken.token)
           console.log('comparative ' + JSON.stringify(comparative.role))
           { comparative.role === 'ADMIN' ? history.push('/admin') : history.push('/user') }
        }        
        await props.changeRegister()
    }

    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            }}>

            <form onSubmit={props.procesarDatos}>
                <div>
                    <input
                        style={{marginBottom: '5px'}}
                        type="text"
                        placeholder="Email"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                    />
                </div>
                <div>
                    <input
                        style={{marginBottom: '5px'}}
                        type="text"
                        placeholder="Password"
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                </div>
                <div>
                    <button style={{
                            backgroundColor: 'Transparent',
                            border: 'none',
                            color: 'black',
                            width: '185px',
                            padding: '20px 16px',
                            textAlign: 'center',
                            textDecoration: 'none',
                            fontSize: '14px',
                        }}
                        type="submit"
                        onClick={signIn}
                        >
                        Log In
                    </button>
                </div>
                <div style={{
                        backgroundColor: 'Transparent',
                        border: 'none',
                        color: 'gray',
                        padding: '10px 16px',
                        textAlign: 'center',
                        textDecoration: 'none',
                        fontSize: '14px',
                        }}
                    onClick={() => props.changeRegister()}
                    >
                    Not registered yet?
                </div>
            </form>
        </div>
    )
}

export default SignIn

/* import { useHistory } from 'react-router-dom'
const history = useHistory();
history.push('/admin')
history.push('/user') */
/*history.push('/user')
        console.log('datos de usuario ' + JSON.stringify(userData))
        changeRegister()
        const urlLogin = "http://localhost:3000/api/login";
        console.log(JSON.stringify(urlLogin))
        return fetch(
            urlLogin, {
                body: JSON.stringify(userData),
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            })
            .then(res => console.log('response ' + JSON.stringify(res)))
            .then(response => response.json())
            .catch((err) => {console.log(err)}); */
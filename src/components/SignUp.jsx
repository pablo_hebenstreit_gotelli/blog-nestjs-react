import React from 'react'

const SignUp = ({changeRegister, procesarDatos}) => {

    const [email, setEmail] = React.useState('')
    const [password, setPaswsword] = React.useState('')
    const [name, setName] = React.useState('')
    const [nickname, setNickname] = React.useState('')
    //const [error, setError] = React.useState(null)
    //const [registered, setRegistered] = React.useState(false)

    const userData = { email, password, name, nickname }

    /* React.useEffect(() => {
        signUp()
    }, []) */

    const signUp = async () => {
        console.log(userData)
        changeRegister()
        const urlSignUp = "http://localhost:3000/api/sign-up";
        return fetch(
            urlSignUp, {
                body: JSON.stringify(userData),
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            })
            .then(response => response.json())
            .catch((err) => {console.log(err)});
        /* const data = await fetch('https://localhost:3000/api/sign-up',
        {
            method: 'POST',
            mode: 'no-cors',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-type': 'aplication/json',
            },
            body: JSON.stringify(userData)
        })
        const dataApi = await data.json()
        console.log(dataApi)
        console.log(data) */
      }

    

    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            }}>
            
            <form onSubmit={procesarDatos}>
            <div>
                <input
                    style={{marginBottom: '5px'}}
                    type="text"
                    placeholder="Email"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
            </div>
            <div>
                <input
                    style={{marginBottom: '5px'}}
                    type="text"
                    placeholder="Password"
                    value={password}
                    onChange={(event) => setPaswsword(event.target.value)}
                />
            </div>
            <div>  
                <input
                    style={{marginBottom: '5px'}}
                    type="text"
                    placeholder="Name"
                    value={name}
                    onChange={(event) => setName(event.target.value)}
                />
                </div>
                <div>
                <input
                    style={{marginBottom: '5px'}}
                    type="text"
                    placeholder="Nickname"
                    value={nickname}
                    onChange={(event) => setNickname(event.target.value)}
                />
                </div>
                <div>
                    <button  
                    style={{
                        backgroundColor: 'Transparent',
                        border: 'none',
                        color: 'black',
                        width: '185px',
                        padding: '20px 16px',
                        textAlign: 'center',
                        textDecoration: 'none',
                        fontSize: '14px',
                    }}
                    type="submit"
                    //onClick={() => props.changeRegister()}
                    onClick={() => signUp()}
                    >
                    Sing up
                    </button>
                </div>
                <div style={{
                            backgroundColor: 'Transparent',
                            border: 'none',
                            color: 'gray',
                            padding: '10px 16px',
                            textAlign: 'center',
                            textDecoration: 'none',
                            fontSize: '14px',
                        }}
                            onClick={() => changeRegister()}
                        >
                            Back to login.
                    </div>
            </form>
        </div>
    )
}

export default SignUp

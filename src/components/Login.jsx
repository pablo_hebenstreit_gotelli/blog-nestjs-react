import React from 'react'
import {withRouter} from 'react-router-dom'
import SignUp from './SignUp'
import SignIn from './SignIn'
import logo from '../blog.png'

const Login = (props) => {
    
    const [registered, setRegistered] = React.useState(true)

    console.log('Registered ' + registered)
    
    const changeRegister = () => {
        console.log('changeRegister')
        setRegistered(!registered)
    }

    return (

        <div className="center" style={{ 
            backgroundImage: `url(${logo})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'center'
          }}>

            {/* <div style={{color: 'black', textAlign: 'center', fontSize: 'xx-large'}}>
                <h1 id="titulo">Blog!</h1> 
            </div> */}
            

            <div className="center-div">

                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    minHeight: '100vh',
                    }}>

                    <h3>

                        {
                            registered ? <SignIn changeRegister={changeRegister}/> : <SignUp changeRegister={changeRegister} />
                        }

                    </h3><br></br>

                </div>

            </div> 

        </div>

    )

}

export default withRouter(Login)